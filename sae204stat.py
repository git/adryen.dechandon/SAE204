import pandas
pandas.__version__
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
pd.__version__

#Lecture du fichier csv
df = pd.read_csv("spotify_songs.csv") 
print(df.head(5))
df = df.dropna()

#Trie de la liste par artist
sorted_df = df.sort_values(by='track_artist')
artist = sorted_df["track_artist"]
print(artist)

#creation d'une liste des artistes
artistes = []
for artiste in sorted_df['track_artist'].unique():
    artistes.append(artiste)

print(artistes)

plt.figure(figsize=(10, 6))

names = sorted_df["track_name"]
popularity = sorted_df["track_popularity"]

# Filtering popularity greater than or equal to 90
filtered_names = [name for name, pop in zip(names, popularity) if pop >= 90]
filtered_popularity = [pop for pop in popularity if pop >= 90]

# Plotting the bar graph
plt.barh(filtered_names,filtered_popularity, color='skyblue')
plt.xlabel('Indice de popularité')
plt.ylabel('Chansons')
plt.title('Analyse des indices de popularité des chansons')
plt.grid(True)
plt.show()
