import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Afficher la version de pandas
print(pd.__version__)

# Lecture du fichier csv
df = pd.read_csv("spotify_songs.csv")
print(df.head(5))

# Supprimer les lignes avec des valeurs manquantes
df = df.dropna()

# Trier le DataFrame par artiste
sorted_df = df.sort_values(by='track_artist')
artistes = sorted_df["track_artist"].unique()

# Créer une liste des artistes uniques
print(artistes)

# Créer une figure pour le premier graphique
plt.figure(figsize=(10, 6))

# Extraire les noms des chansons et leur popularité
names = sorted_df["track_name"]
popularity = sorted_df["track_popularity"]

# Filtrer les chansons avec une popularité >= 90
filtered_names = [name for name, pop in zip(names, popularity) if pop >= 90]
filtered_popularity = [pop for pop in popularity if pop >= 90]

# Tracer le premier graphique en barres horizontales
plt.barh(filtered_names, filtered_popularity, color='skyblue')
plt.xlabel('Indice de popularité')
plt.ylabel('Chansons')
plt.title('Analyse des indices de popularité des chansons')
plt.grid(True)
plt.show()

# Créer un dictionnaire pour stocker la popularité par artiste
popularity_by_artist = {}

# Remplir le dictionnaire avec les indices de popularité correspondant à chaque artiste
for name, pop in zip(names, popularity):
    artiste = sorted_df.loc[sorted_df['track_name'] == name, 'track_artist'].iloc[0]
    if artiste not in popularity_by_artist:
        popularity_by_artist[artiste] = []
    popularity_by_artist[artiste].append(pop)

# Créer un dictionnaire pour stocker la popularité moyenne par artiste
average_popularity_by_artist = {}

# Calculer la popularité moyenne pour chaque artiste
for artiste, popularity_list in popularity_by_artist.items():
    average_popularity = np.mean(popularity_list)
    average_popularity_by_artist[artiste] = average_popularity

# Filtrer les artistes avec une popularité moyenne supérieure à 85
selected_artists = {artiste: popularity for artiste, popularity in average_popularity_by_artist.items() if popularity > 80}

# Créer un graphique pour la deuxième analyse
plt.figure(figsize=(10, 6))

# Tracer un graphique des indices de popularité moyens des artistes sélectionnés
plt.bar(selected_artists.keys(), selected_artists.values(), color='skyblue')

plt.xlabel('Artistes')
plt.ylabel('Popularité moyenne')
plt.title('Analyse de la popularité moyenne des artistes (Popularité moyenne > 85)')
plt.grid(True)
plt.xticks(rotation=90)  # Pour faire pivoter les étiquettes des axes x
plt.tight_layout()  # Ajuster automatiquement les sous-graphiques pour éviter les chevauchements
plt.show()
